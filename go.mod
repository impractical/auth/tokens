module impractical.co/auth/tokens

require (
	darlinggo.co/pan v0.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/gobuffalo/packr v1.13.5 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/hashicorp/go-uuid v1.0.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.9.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rubenv/sql-migrate v0.0.0-20180704111356-3f452fc0ebeb
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/gorp.v1 v1.7.1 // indirect
	impractical.co/pqarrays v0.0.0-20170820231347-970404683d98
	yall.in v0.0.1
)
